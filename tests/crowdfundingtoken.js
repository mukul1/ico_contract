require('babel-polyfill');
var ZohemDataToken = artifacts.require('ZohemDataToken');
import * as utils from "./utils.js";
const InvalidAddress = '0x0';
contract('ZohemDataToken',function(accounts){
	///the array is able to hold a lot of users
	////very important test case
	it('verifies the funding endBlock has been set',async()=>{
		let token = await ZohemDataToken.new(1,20000);
		let endBlock = await token.fundingEndBlock.call();
		assert.equal(endBlock,20000);
	});
	it('verifies the funding startBlock has been set',async()=>{
		let token = await ZohemDataToken.new(1,20000);
		let startBlock = await token.fundingStartBlock.call();
		assert.equal(startBlock,1);
	});
	it('should have an owner', async function() {
	  let token = await ZohemDataToken.new(1,20000);
	  let owner = await token.owner();
	  assert.isTrue(owner !== 0);
	});
	////as the current coinbase change it when testing as your current coinbase 
	it('verifies the owner has been set',async()=>{
		let token = await ZohemDataToken.new(1,20000);
		let owner = await token.owner.call();
		assert.equal(owner,web3.eth.coinbase);
	});
	/**it("should send the ethers to the contract and recieve new coins of ICO",async()=>{
		let token = await ZohemDataToken.new(1,20000);
		await web3.eth.sendTransaction({from:web3.eth.accounts[5],to:token.address,value:10000});
		let bal = await token.balanceOf.call(web3.eth.accounts[5]);
		assert.equal(bal,10000);
	});**/
	///this balance is the newly created ICO tokens
	/**it("should return the balance of the person as correct",async()=>{
		let token = await ZohemDataToken.new(1,20000);
		let bal = await token.balanceOf.call(web3.eth.accounts[5]);
		assert.equal(bal,0);
	});**/
	it("should return the contract balance as correct balance",async()=>{
		let token = await ZohemDataToken.new(1,20000);
		await web3.eth.sendTransaction({from:web3.eth.accounts[5],to:token.address,value:10000,gasLimit:10000000,gas:3000000});
		let bal = await web3.eth.getBalance(token.address);
		assert.equal(bal,10000);
	});
	///as the current start and end blocks a re 1 and 20000 respectively according
	///to the code
	it("should return the current State", async()=>{
		let token = await ZohemDataToken.new(1,20000);
		let st = await token.getState();
		assert.equal(st,1);
	});
	it("should get the value of stopped variable",async()=>{
		let token = await ZohemDataToken.new(1,20000);
		let vari = await token.stopped.call();
		assert.equal(vari,0);
	});
	it("should set the value of stopped variable",async()=>{
		let token = await ZohemDataToken.new();
		let vari = await token.emergencyStop();
		let vario = await token.stopped.call();
		assert.equal(vario,1);
	});
	it("should change the owner valid only by the current owner",async()=>{
		const token = await ZohemDataToken.new(1,20000);
		await token.transferOwnership(accounts[3]);
		const newowner = await token.owner();
		assert.equal(newowner,accounts[3]);
	});
	it("get state returns PreFunding ie 0",async()=>{
		let token = await ZohemDataToken.new(web3.eth.blockNumber+10,web3.eth.blockNumber+15);
		let st = await token.getState();
		assert.equal(st,0);
	});
	///should disallow transfers other than the crowdfunding is a success
	it("should not transfer (send transaction) the amount in the crowdfunding state Of PreFunding",async()=>{
		let token = await ZohemDataToken.new(web3.eth.blockNumber+10,web3.eth.blockNumber+15);
		//
		try{
			await web3.eth.sendTransaction({from:web3.eth.accounts[5],to:token.address,value:10000});
			assert(false,'didnt throw');
		}catch(error){
			return error;
		}
	});
	it("should test for the transfer allowing in the funding state just for test not allowed",async()=>{
		let token = await ZohemDataToken.new(1,20000);
		await web3.eth.sendTransaction({from:web3.eth.accounts[5],to:token.address,value:10000,gasLimit:10000000,gas:3000000});
		try{
			await token.transfer(web3.eth.coinbase,100);
			assert(false,'didnt throw');
		}catch(error){
			return error;
		}
	});
	it("should return the total supply",async()=>{
		let token = await ZohemDataToken.new(1,20000);
		await web3.eth.sendTransaction({from:web3.eth.accounts[5],to:token.address,value:10000,gasLimit:10000000,gas:3000000});
		let supply = await token.totalSupply.call();
		assert.equal(supply.toNumber(),440000);
	});
	it("should disallow finalize crowdfunding in Prefunding State",async()=>{
		let token = await ZohemDataToken.new(web3.eth.blockNumber+10,web3.eth.blockNumber+15);
		try{
			await token.finalizedCrowdfunding.call();
			assert(false,'didnt throw');
		}catch(error){
			return error;
		}
	});
	it("should disallow finalize crowdfunding in Funding State",async()=>{
		let token = await ZohemDataToken.new(1,20000);
		try{
			await token.finalizedCrowdfunding.call();
			assert(false,'didnt throw');
		}catch(error){
			return error;
		}
	});
	it("should allow finalize crowdfunding in Funding State",async()=>{
		let token = await ZohemDataToken.new(1,20000);
		try{
			await token.finalizeCrowdfunding();
			assert(false,'didnt throw');
		}catch(error){
			return error;
		}
	});
	it('should not allow sending to invalid address',async()=>{
		let token = await ZohemDataToken.new(1,20000);
		try{
			await token.transfer(InvalidAddress,100);
			assert(false,'didnt throw');
		}catch(error){
			return error;
		}
	});
	it('Should verify the spending amount after allowance',async()=> {
		let token = await ZohemDataToken.new(1,20000);
		await token.approve(accounts[1],100);
		let allowance = await token.allowance.call(accounts[0],accounts[1]);
		assert.equal(allowance,100);
	});
	it("should check the current btc relay address as 0",async()=>{
		let token = await ZohemDataToken.new(1,20000);
		let ans = await token.trustedBTCRelay.call();
		assert.equal(ans,'0x0000000000000000000000000000000000000000');
	});
	it("should be able to set btcrelayaddress",async()=>{
		let token = await ZohemDataToken.new(1,20000);
		let var1 = await token.setTrustedBtcRelay.call('0x0000000000000000000000000000000000000011');
		assert.equal(var1,true);
	});
	it("should be able to set btcrelayaddress and return correct address",async()=>{
		let token = await ZohemDataToken.new(1,20000);
		await token.setTrustedBtcRelay('0x0000000000000000000000000000000000000011');
		let var1 = await token.trustedBTCRelay.call();
		assert.equal(var1,'0x0000000000000000000000000000000000000011');
	});
	////for the successful state
	/**it("get state should return success after tokenCreationmax",async()=>{
		let token = await ZohemDataToken.new(web3.eth.blockNumber + 2,web3.eth.blockNumber + 4);
		await utils.mineToBlockHeight(web3.eth.blockNumber + 2);
		let maxcreation = await token.tokenCreationMax.call();
		//console.log(maxcreation);
		let exchangeRate = await token.tokensPerEther.call();
		let totalTokensForSuccess = await maxcreation/exchangeRate;
		//assert.equal(totalTokensForSuccess,100);
		await web3.eth.sendTransaction({from:web3.eth.accounts[5],to:token.address,value:10000,gasLimit:10000000,gas:3000000});
		await token.finalizedCrowdfunding();
		let state = await token.getState();
		assert.equal(state.toNumber(),2);

	});**/
	it("get state should return Success after funding end block ends",async()=>{
		let token = await ZohemDataToken.new(web3.eth.blockNumber + 2,web3.eth.blockNumber + 4);
		await utils.mineToBlockHeight(web3.eth.blockNumber + 2);
		let maxcreation = await token.tokenCreationMax.call();
		let exchangeRate = await token.tokensPerEther.call();
		let totalTokensForSuccess = await maxcreation.div(exchangeRate);
		assert.equal(totalTokensForSuccess.toNumber(), 227272727272727280000);
	});
	/**it("should run the process transaction that will be called by the btcrelay contract",async()=>{
		let token = await ZohemDataToken.new(1,20000);
		await token.processTransaction("0100000001c9ff4d3d767af9d9181eab58309d789bd75d38f341c668068fb06ef9827ba91d000000006a47304402201d171b0f8629d1d0d1f735d70b7443809b3773400d1f5a40292ada937726dc7a022008fd8da1640c3e72c3dfc293e795e1cda2647f1fef0c20039ae91e594b50da0c01210202f1b5e9bc11bcb09a9e054a8ea77837c162509822fbba252ab8bfa3d6a9c93fffffffff022f380200000000001976a914a0dc485fc3ade71be5e1b68397abded386c0adb788ac10270000000000001976a914239eb5afa5b1da814a3ccb60e79de1f933f1b47088ac00000000","1000000000000000000000000000");
		let val = token.tx1satoshis.call();
		assert.equal(val,1000);
	});*/
	it("should not allocate any tokens right after the transaction is done",async()=>{
		let token = await ZohemDataToken.new(1,20000);
		await web3.eth.sendTransaction({from:web3.eth.accounts[5],to:token.address,value:10000,gasLimit:10000000,gas:3000000});
		let bal = await token.balanceOf.call(web3.eth.accounts[5]);
		assert.equal(bal,0);
	});
	it("array of global addresses should contain the address correctly after tx",async()=>{
		let token = await ZohemDataToken.new(1,20000);
		await web3.eth.sendTransaction({from:web3.eth.accounts[5],to:token.address,value:10000,gasLimit:10000000,gas:3000000});
		let val = await token.getarr.call(0);
		assert.equal(val,web3.eth.accounts[5]);
	});
	it("the address returned from the array should have the right struct ie tokens",async()=>{
		let token = await ZohemDataToken.new(1,20000);
		await web3.eth.sendTransaction({from:web3.eth.accounts[5],to:token.address,value:10000,gasLimit:10000000,gas:3000000});
		let val = await token.getarr.call(0);
		let vala = await token.getTokens.call(val);
		assert.equal(vala.toNumber(),440000);
	});
	it("after the finalethercall is called the new tokens are transferred to the 1 person",async()=>{
		let token = await ZohemDataToken.new(1,20000);
		await web3.eth.sendTransaction({from:web3.eth.accounts[5],to:token.address,value:10000,gasLimit:10000000,gas:3000000});
		await token.finalEtherDistribution();
		let val = await token.balanceOf.call(web3.eth.accounts[5]);
		assert.equal(val.toNumber(),440000);
	});
		it("after the finalethercall is called the new tokens are transferred to the 2 persons",async()=>{
		let token = await ZohemDataToken.new(1,20000);
		await web3.eth.sendTransaction({from:web3.eth.accounts[5],to:token.address,value:10000,gasLimit:10000000,gas:3000000});
		await web3.eth.sendTransaction({from:web3.eth.accounts[4],to:token.address,value:1000,gasLimit:10000000,gas:3000000});
		await token.finalEtherDistribution();
		let val = await token.balanceOf.call(web3.eth.accounts[5]);
		let val1 = await token.balanceOf.call(web3.eth.accounts[4]);
		assert.equal(val.toNumber(),440000);
		assert.equal(val1.toNumber(),44000);
	});
	///to do final run test case
	it("should be able to hold the large number and call final ether without fail",async()=>{
		let token = await ZohemDataToken.new(1,20000);
		for(var i=0;i<20000;i++)
		{
			await web3.eth.sendTransaction({from:web3.eth.accounts[4],to:token.address,value:1,gasLimit:10000000,gas:3000000});
		}
		let f = await token.getArrLen.call();
		let c=  await  token.getTotalepochs.call();
		assert.equal(f,20000);
		for(var i=1;i<=c;i++)
		{
			await token.finalEtherDistribution(i);
		}

	});
	
});