require('babel-polyfill');
var TestERC20Token = artifacts.require('TestERC20Token');
var ERC20Token = artifacts.require('ERC20Token');
const InvalidAddress = '0x0';
contract('TestERC20Token',function(accounts){
	it('verifies the token name after construction', async () => {
	        let token = await TestERC20Token.new('Mukul', 'muk', 0);
	        let name = await token.name.call();
	        assert.equal(name, 'Mukul');
	    });
	it('verifies the Token Symbol', async () => {
	        let token = await TestERC20Token.new('Mukul', 'muk', 0);
	        let symbol = await token.symbol.call();
	        assert.equal(symbol, 'muk');
	    });
	it('verifies the total SUpply', async () => {
	        let token = await TestERC20Token.new('Mukul', 'muk', 1000);
	        await token.setTotalSupply.call(1000);
	        let supply = await token.totalSupply.call();
	        assert.equal(supply,1000);
	    });
	it('verifies the balances after transfer', async() =>{
		let token = await TestERC20Token.new('Mukul','muk',1000);
		await token.transfer(accounts[1],400);
		let balance;
		balance = await token.getBalance.call(accounts[0]);
		assert.equal(balance,600);
	});
	it('should not allow sending more than balance',async() =>{
		let token = await TestERC20Token.new('mukul','muk',1000);
		try{
			await token.transfer(accouns[1],5000);
			assert(false,'didnt throw');
		}catch(error){
			return error;
		}
	});
	it('should not allow sending to invalid address',async()=>{
		let token = await TestERC20Token.new('mukul','muk',1000);
		try{
			await token.transfer(InvalidAddress,100);
			assert(false,'didnt throw');
		}catch(error){
			return error;
		}
	});
	it('Should verify the spending amount after allowance',async()=> {
		let token = await ERC20Token.new('mukul','muk',1000);
		await token.approve(accounts[1],100);
		let allowance = await token.allowance.call(accounts[0],accounts[1]);
		assert.equal(allowance,100);
	});

	




})