var BTC= artifacts.require("./BTC.sol");
var ZohemDataToken = artifacts.require('./ZohemDataToken');
module.exports = function(deployer) {
	deployer.deploy(BTC);
	deployer.link(BTC,ZohemDataToken);
 	deployer.deploy(ZohemDataToken,1,2000);
};
